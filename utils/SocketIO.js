/**
 * Created by qasim on 12/8/15.
 */

function SocketIO(server) {
    this.io = require("socket.io")(server);
    var sockets = this.io;
    this.io.on("connection", function (socket) {
        socket.on("on_client_connect", function (data) {
            //console.log("new client connection" + socket.id);
            sockets.sockets.emit("new_device_connection", {"socket": socket.id});
        });
        socket.on("update", function (data) {
            counterEvent(sockets,"update",{"seconds": parseInt(data) / 1000})
        });
        socket.on("start", function (data) {
            //console.log(data);
            counterEvent(sockets, "start", {"seconds": parseInt(data.seconds) * 1000,"colorData":data.colorData});
        });
        socket.on("stop", function (data) {
            //console.log(data);
            counterEvent(sockets, "stop", {"seconds": parseInt(data.seconds) * 1000});

        });
        socket.on("reset", function (data) {
            //console.log(data);
            counterEvent(sockets, "reset", {"seconds": parseInt(data.seconds) * 1000,"colorData":data.colorData});
        });

        socket.on("font-size-change", function (data) {
            //console.log(data);
            counterEvent(sockets, "font-size-change", {"size": data.fontSize});
        });

        //console.log("Device Connected With Id "+socket.id);
    });

}

function counterEvent(io, e, args) {
    //if (io.sockets.connected[args.socket]) {
    //    io.sockets.connected[args.socket].emit(e, args);
    //}

    io.sockets.emit(e, args);

}

module.exports = SocketIO;
